 ! fortran   f90
 
 ! To use it with python, compile with f2py in lib as follows:
 !  cd carlo/codes/einstein/lib
 !  f2py -c  -m fortmsd fortmsd.f90 
 ! 
 
 
SUBROUTINE calculate_msd( &
    positions, atoms, species_count, stepsize,  &
    timestep_in_fs, block_length_in_fs, &
    maxtime_in_fs, vaf_is_0, &
    nsp, nstep, nat)


! Leave nstep and nat last in declaration, otherwise it won't work,,, but why???

    implicit none
    INTEGER, intent(in)         :: nstep,nat             ! Number of timesteps, number of atoms, stepsize
    REAL*8, intent(in)          :: timestep_in_fs, block_length_in_fs, vaf_is_0, maxtime_in_fs   ! timestep given in femtoseconds

    INTEGER, intent(in)         :: stepsize, nsp
    REAL*8, intent(in)          :: positions(nstep, nat, 3)

    INTEGER, intent(in)         :: atoms(nat)
    INTEGER, intent(in)         :: species_count(nsp)

    ! Local variables:
    INTEGER                     ::  t, tau, nr_of_t,           &        ! Time variables
                                    iat,isp,                   &        ! Atom/species count
                                    nr_of_blocks,               &
                                    iblock,                     &        !  Block analysis
                                    block_length_dt, maxtime_in_dt

!~     INTEGER :: counter
    REAL*8         ::  N
    REAL*8, allocatable   :: local_positions(:,:,:)
    REAL*8, allocatable   :: msd(:, :, :)

    block_length_dt = int(block_length_in_fs /  timestep_in_fs)
    N = DBLE(block_length_dt)
    nr_of_blocks = 0

    nr_of_blocks =  nstep / block_length_dt - 1
    nr_of_t      =  block_length_dt / stepsize + 1

    allocate(msd(nsp, nr_of_blocks, nr_of_t))
    allocate(local_positions(nstep, nat, 3))
    local_positions(:,:,:) = positions(:,:,:)

    maxtime_in_dt = int(maxtime_in_fs /  timestep_in_fs)

    print*, 'nstep               =  ', nstep
    print*, 'nat                 =  ', nat
    print*, 'nsp                 =  ', nsp
    print*, 'stepsize            =  ', stepsize
    print*, 'timestep_in_fs      =  ', timestep_in_fs
    print*, 'block_length_in_fs  =  ', block_length_in_fs
    print*, 'block_length_dt     =  ', block_length_dt
    print*, 'vaf is 0            =  ', vaf_is_0
    print*, 'maxtime_in_fs       =  ', maxtime_in_fs
    print*, 'maxtime_in_dt       =  ', maxtime_in_dt
    print*, 'N                   =  ', N
    print*, 'nr_of_blocks        =  ', nr_of_blocks
    print*, 'nr_of_t             =  ', nr_of_t
    
    print*, atoms
    print*, species_count

    print*, 'STARTING'

!~     DO block_start = 0, nstep - 2*block_length_dt , block_length_dt
    DO iat=1,nat
        isp = atoms(iat)
        print*, isp
        DO iblock = 1, nr_of_blocks
            DO t = 1, nr_of_t
                msd(isp, iblock, t) = 0.0d0
                DO tau = (iblock -1)*block_length_dt+1, iblock*block_length_dt
                
                    msd(isp, iblock, t) = msd(isp, iblock, t) + &
                        (local_positions(tau+stepsize*t, iat, 1)-local_positions(tau, iat, 1))**2+&
                        (local_positions(tau+stepsize*t, iat, 2)-local_positions(tau, iat, 2))**2+&
                        (local_positions(tau+stepsize*t, iat, 3)-local_positions(tau, iat, 3))**2
                END DO
            END DO
            DO isp=1, nsp
                msd(isp, iblock, t) = msd(isp, iblock, t) / N / DBLE(species_count(isp))
            END DO
        END DO
    END DO


    deallocate(local_positions)
    deallocate(msd)

end SUBROUTINE calculate_msd

SUBROUTINE calculate_msd_single_species(    &
    positions,                              &
    msd,                                    &
    stepsize,                               &
    block_length_dt,                        &
    nr_of_blocks,                           &
    nr_of_t,                                &
    nstep,                                  &
    nat                                     &
)

    implicit none
    INTEGER, intent(in)         ::  nstep,  nat,            &  ! Number of timesteps, number of atoms, stepsize
                                    nr_of_blocks, nr_of_t,  &
                                    block_length_dt, stepsize    
    ! THE ARRAYS:
    REAL*8, intent(in),  dimension(nstep, nat, 3)           :: positions
    REAL*8, intent(out), dimension(nr_of_blocks, nr_of_t)   :: msd

    ! Local variables:
    INTEGER                     ::  t, tau,                    &        ! Time variables
                                    iat,                       &        ! Atom count
                                    iblock                              ! block count
    REAL*8                      ::  msd_this_t


    print*, " This is fortmsd.calculate_msd_single_species,"
    print*, " These are my arguments:"
    print*,
    
    print*, '   nstep               =  ', nstep
    print*, '   nat                 =  ', nat
    print*, '   stepsize            =  ', stepsize
    print*, '   block_length_dt     =  ', block_length_dt
    print*, '   nr_of_blocks        =  ', nr_of_blocks
    print*, '   nr_of_t             =  ', nr_of_t
    
    ! Loop over the block
    DO iblock = 1, nr_of_blocks
        ! Loop over time t
        print*, "At block", iblock
        DO t = 1, nr_of_t
            ! Calculate one value of msd for this t in this block 
            ! based on the average over atoms and running window defined by tau
            msd_this_t = 0.0d0
            ! loop over atoms (remember this code only deals with single species
            DO iat=1,nat
                
                ! Running average achieved by letting tau run through the whole block
                
                DO tau = (iblock -1)*block_length_dt+1, iblock*block_length_dt
                
                    msd_this_t = msd_this_t + &
                        (positions(tau+stepsize*t, iat, 1)-positions(tau, iat, 1))**2+&
                        (positions(tau+stepsize*t, iat, 2)-positions(tau, iat, 2))**2+&
                        (positions(tau+stepsize*t, iat, 3)-positions(tau, iat, 3))**2
                
                END DO
            END DO
            msd(iblock, t) = msd_this_t  
        END DO
    END DO

    ! The number of samples is the same for all calculated value

    msd(:,:) = msd(:, :) / DBLE(block_length_dt) / DBLE(nat)
    print*, "fortmsd.calculate_msd_single_species FINISHED"
    print*, "    FORTRAN OUT   "

end SUBROUTINE calculate_msd_single_species


SUBROUTINE calculate_msd_specific_atoms(    &
    positions,                              &
    indices_of_interest,                    &
    msd,                                    &
    stepsize,                               &
    block_length_dt,                        &
    nr_of_blocks,                           &
    nr_of_t,                                &
    nstep,                                  &
    nat,                                    &
    nat_of_interest                         &
)

    ! Fastest routine here, instead of manipulating the trajectories 
    ! and passing only the trajectories of interest, 
    ! pass the indices of interest (remember index counting starts at 1)
    ! and let fortran do the magic
    ! Faster (x2) because no array manipulation is necessary

    implicit none
    INTEGER, intent(in)         ::  nstep,  nat,            &   ! Number of timesteps, number of atoms
                                    nat_of_interest,        &   ! number of atoms of interest
                                    nr_of_blocks,           &   ! Nr of blocks to calculate
                                    block_length_dt,        &   ! block length in dt
                                    nr_of_t,                &   ! nr of times of t
                                    stepsize                    ! The stepsize, eg 10 to calculate every 10th time

    ! THE ARRAYS:
    REAL*8, intent(in),  dimension(nstep, nat, 3)           :: positions
    INTEGER, intent(in), dimension(nat_of_interest)         :: indices_of_interest
    REAL*8, intent(out), dimension(nr_of_blocks, nr_of_t)   :: msd

    ! Local variables:
    INTEGER                     ::  t, tau,                    &        ! Time variables
                                    iat,                       &        ! Atom count
                                    iat_of_interest,           &        ! Atom count in indices_of interest
                                    iblock                              ! block count
    REAL*8                      ::  msd_this_t

!~ 
!~     print*, " This is fortmsd.calculate_msd_single_species,"
!~     print*, " These are my arguments:"
!~     print*,
!~     print*, '   nstep               =  ', nstep
!~     print*, '   nat                 =  ', nat
!~     print*, '   stepsize            =  ', stepsize
!~     print*, '   block_length_dt     =  ', block_length_dt
!~     print*, '   nr_of_blocks        =  ', nr_of_blocks
!~     print*, '   nr_of_t             =  ', nr_of_t
    
    ! Loop over the block
    DO iblock = 1, nr_of_blocks
        ! Loop over time t
        DO t = 1, nr_of_t
            ! Calculate one value of msd for this t in this block 
            ! based on the average over atoms and running window defined by tau
            msd_this_t = 0.0d0
            ! loop over atoms (remember this code only deals with single species
            DO iat_of_interest=1, nat_of_interest
                iat = indices_of_interest(iat_of_interest)
                ! Running average achieved by letting tau run through the whole block
                DO tau = (iblock -1)*block_length_dt+1, iblock*block_length_dt
                    msd_this_t = msd_this_t + &
                        (positions(tau+stepsize*t, iat, 1)-positions(tau, iat, 1))**2+&
                        (positions(tau+stepsize*t, iat, 2)-positions(tau, iat, 2))**2+&
                        (positions(tau+stepsize*t, iat, 3)-positions(tau, iat, 3))**2
                
                END DO
            END DO
            msd(iblock, t) = msd_this_t
        END DO
    END DO

    ! The number of samples is the same for all calculated value

    msd(:,:) = msd(:, :) / DBLE(block_length_dt) / DBLE(nat_of_interest)
!~     print*, "fortmsd.calculate_msd_specific_atoms FINISHED"
!~     print*, "    FORTRAN OUT   "

end SUBROUTINE calculate_msd_specific_atoms
