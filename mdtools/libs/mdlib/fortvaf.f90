
 ! To use it with python, compile with f2py in lib as follows:
 !  cd carlo/codes/einstein/lib
 !  f2py -c  -m fortvaf fortvaf.f90 
 
SUBROUTINE calculate_vaf_specific_atoms(    &
    positions,                              &
    indices_of_interest,                    &
    vaf,                                    &
    stepsize,                               &
    stepsize_tau,                           &
    nr_of_t,                                &
    nstep,                                  &
    nat,                                    &
    nat_of_interest                         &
)

    ! Calcyulates the velocities from the positions and than the autocorrelation function
    ! Averaging over all atoms given by the incices of interest

    implicit none
    INTEGER, intent(in)         ::  nstep,  nat,            &   ! Number of timesteps, number of atoms
                                    nat_of_interest,        &   ! number of atoms of interest
                                    nr_of_t,                &   ! nr of times of t
                                    stepsize,               &   ! The stepsize, eg 10 to calculate every 10th time
                                    stepsize_tau                ! The stepsize for tau, eg 10 to calculate every 10th time

    ! THE ARRAYS:
    REAL*8, intent(in),  dimension(nstep, nat, 3)           :: positions
    INTEGER, intent(in), dimension(nat_of_interest)         :: indices_of_interest
    REAL*8, intent(out), dimension(nr_of_t)                 :: vaf

    ! Local array for velocities
    REAL*8, allocatable                                     :: velocities(:,:,:)

    ! Local variables:
    INTEGER                     ::  t, tau, max_tau,           &        ! Time variables
                                    nr_of_samples,             &
                                    iat,                       &        ! Atom count
                                    iat_of_interest                     ! Atom count in indices_of interest

    REAL*8                      ::  vaf_this_t, nr_of_samples_real

    allocate(velocities(nstep-1,nat_of_interest,3))

    DO iat_of_interest=1, nat_of_interest
        iat = indices_of_interest(iat_of_interest)
        DO t=1, nstep-1
            velocities(t, iat_of_interest,:) = positions(t+1, iat, :) - positions(t, iat, :)
        END DO
    END DO

    max_tau = nstep -stepsize_tau - 1 - nr_of_t*stepsize  ! to avoid index out of bonds

    nr_of_samples = 0
    DO tau = 1, max_tau, stepsize_tau
        nr_of_samples =  nr_of_samples + 1
    END DO
    nr_of_samples = nr_of_samples * nat_of_interest
    nr_of_samples_real = DBLE(nr_of_samples)

    DO t = 1, nr_of_t
        vaf_this_t = 0.0d0
        DO iat=1, nat_of_interest
            DO tau = 1, max_tau, stepsize_tau
                vaf_this_t = vaf_this_t  + &
                    velocities(tau+stepsize*t, iat, 1)*velocities(tau, iat, 1)+&
                    velocities(tau+stepsize*t, iat, 2)*velocities(tau, iat, 2)+&
                    velocities(tau+stepsize*t, iat, 3)*velocities(tau, iat, 3)
            END DO
        END DO
        vaf(t) = vaf_this_t /nr_of_samples_real
    END DO
    deallocate(velocities)


END SUBROUTINE calculate_vaf_specific_atoms


SUBROUTINE calculate_vaf_from_velocities(    &
    velocities,                              &
    indices_of_interest,                    &
    vaf,                                    &
    stepsize,                               &
    stepsize_tau,                           &
    nr_of_t,                                &
    nstep,                                  &
    nat,                                    &
    nat_of_interest                         &
)

    ! Calcyulates the velocities from the positions and than the autocorrelation function
    ! Averaging over all atoms given by the incices of interest

    implicit none
    INTEGER, intent(in)         ::  nstep,  nat,            &   ! Number of timesteps, number of atoms
                                    nat_of_interest,        &   ! number of atoms of interest
                                    nr_of_t,                &   ! nr of times of t
                                    stepsize,               &   ! The stepsize, eg 10 to calculate every 10th time
                                    stepsize_tau                ! The stepsize for tau, eg 10 to calculate every 10th time

    ! THE ARRAYS:
    REAL*8, intent(in),  dimension(nstep, nat, 3)           :: velocities
    INTEGER, intent(in), dimension(nat_of_interest)         :: indices_of_interest
    REAL*8, intent(out), dimension(nr_of_t)                 :: vaf

    ! Local variables:
    INTEGER                     ::  t, tau, max_tau,   &        ! Time variables
                                    nr_of_samples,     &
                                    iat,               &        ! Atom count
                                    iat_of_interest             ! Atom count in indices_of interest

    REAL*8                      ::  vaf_this_t, nr_of_samples_real

!~     allocate(velocities(nstep-1,nat_of_interest,3))
!~ 
!~     DO iat_of_interest=1, nat_of_interest
!~         iat = indices_of_interest(iat_of_interest)
!~         DO t=1, nstep-1
!~             velocities(t, iat_of_interest,:) = positions(t+1, iat, :) - positions(t, iat, :)
!~         END DO
!~     END DO

    max_tau = nstep -stepsize_tau - 1 - nr_of_t*stepsize  ! to avoid index out of bonds
    print*, max_tau, stepsize_tau

    nr_of_samples = 0
    DO tau = 1, max_tau, stepsize_tau
        nr_of_samples =  nr_of_samples + 1
    END DO
    nr_of_samples = nr_of_samples * nat_of_interest
    nr_of_samples_real = DBLE(nr_of_samples)

    DO t = 1, nr_of_t
        vaf_this_t = 0.0d0
        DO iat_of_interest=1, nat_of_interest
            iat = indices_of_interest(iat_of_interest)
!~             print*, iat
            DO tau = 1, max_tau, stepsize_tau
                vaf_this_t = vaf_this_t + &
                    velocities(tau+stepsize*t, iat, 1)*velocities(tau, iat, 1)+&
                    velocities(tau+stepsize*t, iat, 2)*velocities(tau, iat, 2)+&
                    velocities(tau+stepsize*t, iat, 3)*velocities(tau, iat, 3)
            END DO
        END DO
        vaf(t) = vaf_this_t /nr_of_samples_real
    END DO
!~     deallocate(velocities)


END SUBROUTINE calculate_vaf_from_velocities



SUBROUTINE calculate_vaf_specific_atoms_high_acc(    &
    positions,                              &
    indices_of_interest,                    &
    vaf,                                    &
    nr_of_t,                                &
    nstep,                                  &
    nat,                                    &
    nat_of_interest                         &
)

    ! Calcyulates the velocities from the positions and than the autocorrelation function
    ! Averaging over all atoms given by the incices of interest

    implicit none
    INTEGER, intent(in)         ::  nstep,  nat,            &   ! Number of timesteps, number of atoms
                                    nat_of_interest,        &   ! number of atoms of interest
                                    nr_of_t                     ! nr of times of t

    ! THE ARRAYS:
    REAL*8, intent(in),  dimension(nstep, nat, 3)           :: positions
    INTEGER, intent(in), dimension(nat_of_interest)         :: indices_of_interest
    REAL*8, intent(out), dimension(nr_of_t)                 :: vaf

    ! Local array for velocities
    REAL*8, allocatable                                     :: velocities(:,:,:)

    ! Local variables:
    INTEGER                     ::  t, tau,                    &        ! Time variables
                                    nr_of_samples,             &
                                    iat,                       &        ! Atom count
                                    iat_of_interest                     ! Atom count in indices_of interest

    REAL*8                      ::  vaf_this_t

    allocate(velocities(nstep-1,nat_of_interest,3))

    DO iat_of_interest=1, nat_of_interest
        iat = indices_of_interest(iat_of_interest)
        DO t=1, nstep-1
            velocities(t, iat_of_interest,:) = positions(t+1, iat, :) - positions(t, iat, :)
        END DO
    END DO

    DO t = 0, nr_of_t-1
        vaf_this_t = 0.0d0
        nr_of_samples = 0
        DO iat=1, nat_of_interest
            DO tau = 1, nstep-t
                nr_of_samples = nr_of_samples + 1
                vaf_this_t = vaf_this_t  + &
                    velocities(tau+t, iat, 1)*velocities(tau, iat, 1)+&
                    velocities(tau+t, iat, 2)*velocities(tau, iat, 2)+&
                    velocities(tau+t, iat, 3)*velocities(tau, iat, 3)
            END DO
        END DO
        vaf(t+1) = vaf_this_t / DBLE(nr_of_samples)
    END DO
    deallocate(velocities)


END SUBROUTINE calculate_vaf_specific_atoms_high_acc
