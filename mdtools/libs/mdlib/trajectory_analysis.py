# -*- coding: utf-8 -*-
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"
"""
This is the module to calculate mean square displament (MSD),
velocity autocorrelation function (VAF)
and diffusion coefficient (D).

This is mainly taken care of by the class
:func:`TrajectoryAnalyzer` below.
"""

import sys
import numpy as np
import itertools

from scipy.stats import linregress
from scipy.stats import sem as standard_error_of_mean

from ase.data.colors import jmol_colors
from ase.data import atomic_masses, atomic_numbers
from ase.data.colors import jmol_colors

from matplotlib import pyplot as plt
from matplotlib.gridspec  import GridSpec
from colorsys import rgb_to_hls, hls_to_rgb

from tempfile import mkstemp

from carlo.utils.misc import flatten
from carlo.utils.constants import e2, kB
from carlo.codes.voronoi.imports import structure_wizard



class TrajectoryAnalyzer():
    """
    Class analyzing trajectories and sampling statistics
    """
    def __init__(self, **kwargs):
        """
        Initializes an instance of the class TrajectoryAnalyzer.
        Arguments passed can include:
        *   ``log``: A file to write to, else will print to standard output
        *   ``verbosity``: A boolean to determine printing level
        *   ``trajectory``: The trajectory to analyze, calls :func:`set_trajectory`
        *   ``structure``: The structure that the trajectory was started from, mainly to get the atomic species, call to :func:`set_structure`
        """
        self.log = kwargs.get('log', sys.stdout)
        self.verbosity = kwargs.get('verbosity', False)
        if 'trajectory' in kwargs.keys():
            self.set_trajectory(kwargs['trajectory'])
        if 'structure' in kwargs.keys():
            self.set_structure(kwargs['structure'])

    def set_trajectory(self, trajectory, timestep_in_fs):
        """
        Set the trajectory to be analyzed.
        The trajectory needs to be in the same form as return by an instance of Traj in the database.
        Receives

        :param trajectory_dict: A dictionary that contains t
        :param timestep_in_fs: The timestep in fs as a float.


        """
        if not isinstance(trajectory, np.ndarray):
            raise Exception(
                'I was given a {} instead of a numpy array'
                ''.format(type(trajectory))
            )
        self.trajectory = trajectory
        print "       trajectory of shape {}".format(self.trajectory.shape)
        self.timestep_in_fs = timestep_in_fs

    def set_temperature(self, temperature):
        self.temperature = temperature


    def set_structure(self, structure):
        """
        Set the structure with the help of :func:`~carlo.codes.voronoi.structure_imports.structure_wizard`
        """

        try:
            from aiida.orm.data.structure import StructureData
            if isinstance(structure, StructureData):
                self.structure = structure
                self._chemical_symbols = structure.get_site_kindnames()
                #~ print "HERE"
            else:
                raise Exception("Not an instance")
        except Exception as e:
            print e
            s = structure_wizard(structure, need_dict=True)
            self.structure = s['structure']
            self._chemical_symbols = s['chemical_symbols']

    def calculate_vaf_fort(self, **kwargs):
        # HERE
        from lib import fortvaf
        try:
            trajectory     = self.trajectory
            timestep_in_fs = self.timestep_in_fs
            total_steps    = len(trajectory)
            total_time     = total_steps*self.timestep_in_fs
            nstep, nat, _  = trajectory.shape
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_trajectory method to set trajectory"
                "\n\n\n"
            )
        try:
            structure = self.structure
            self._chemical_symbols
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        log = self.log # set in __init__
        self.vaf_all_species =  []

        species_of_interest  = kwargs.get('species_of_interest', None)
        if species_of_interest is None:
            species_of_interest =  list(set(structure['atoms']))

        stepsize_t_vaf = kwargs.get('stepsize_t_vaf', 1)
        stepsize_tau_vaf = kwargs.get('stepsize_tau_vaf', 1)

        max_t_vaf_fs  = kwargs.get('max_t_vaf_fs', None)
        if not isinstance(max_t_vaf_fs, (int, float)):
            raise Exception(
                "\n\n\n"
                "Please set the time to calculate VAF for (in femtoseconds)"
                "\n\n\n"
            )

        max_t_vaf_dt  = int(max_t_vaf_fs / timestep_in_fs)
        nr_of_t        = max_t_vaf_dt / stepsize_t_vaf


        for atomic_species in species_of_interest:
            indices_of_interest = np.array(
                [
                    index
                    for index, atom
                    in enumerate(self._chemical_symbols)
                    if atom == atomic_species
                ],
                dtype='i'
            )
            # Python to fortran index i -> i+1
            indices_of_interest = indices_of_interest + 1
            nat_of_interest     = len(indices_of_interest)

            log.write(
                '\n    ! Calculating VAF for atomic species {}\n'
                '      Structure contains {} atoms of type {}\n'
                '      Max time (fs)     = {}\n'
                '      Max time (dt)     = {}\n'
                '      Stepsize for t    = {}\n'
                '      stepsize for tau  = {}\n'
                '      nr of timesteps   = {}\n'
                '      Calculating VAF with fortran subroutine fortvaf.calculate_vaf_specific_atoms\n'
                ''.format(
                    atomic_species,
                    nat_of_interest,
                    atomic_species,
                    max_t_vaf_fs,
                    max_t_vaf_dt,
                    stepsize_t_vaf,
                    stepsize_tau_vaf,
                    nr_of_t,
                )
            )


            vaf = fortvaf.calculate_vaf_specific_atoms(
                trajectory,
                indices_of_interest,
                stepsize_t_vaf,
                stepsize_tau_vaf,
                nr_of_t,
                nstep,
                nat,
                nat_of_interest
            )
            log.write(
                "      calculate_vaf_specific_atoms, returned array of length {}\n"
                "".format(len(vaf))
            )
            self.vaf_all_species.append(vaf)
        self.vaf_results_dict = dict(
            max_t_vaf_fs    =   max_t_vaf_fs,
            max_t_vaf_dt    =   max_t_vaf_dt,
            stepsize_t_vaf  =   stepsize_t_vaf,
            stepsize_tau_vaf    =   stepsize_tau_vaf,
            timestep_in_fs  =   timestep_in_fs,
            nr_of_t         =   nr_of_t,
            species_of_interest=species_of_interest,
        )
        return self.vaf_results_dict, np.array(self.vaf_all_species)
    def calculate_vaf_fort_from_velocities(self, velocities, timestep_in_fs, **kwargs):
        # HERE
        from lib import fortvaf
        total_steps    = len(velocities)
        total_time     = total_steps*timestep_in_fs
        nstep, nat, _  = velocities.shape

        try:
            structure = self.structure
            self._chemical_symbols
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        log = self.log # set in __init__
        self.vaf_all_species =  []

        species_of_interest  = kwargs.get('species_of_interest', None)
        if species_of_interest is None:
            species_of_interest =  list(set(structure['atoms']))

        stepsize_t_vaf        = kwargs.get('stepsize_t_vaf', 1)
        stepsize_tau_vaf    = kwargs.get('stepsize_tau_vaf', 1)

        max_t_vaf_fs  = kwargs.get('max_t_vaf_fs', None)
        if not isinstance(max_t_vaf_fs, float):
            raise Exception(
                "\n\n\n"
                "Please set the time to calculate VAF for (in femtoseconds)"
                "\n\n\n"
            )
        max_t_vaf_dt  = int(max_t_vaf_fs / timestep_in_fs)
        nr_of_t        = max_t_vaf_dt / stepsize_t_vaf


        for atomic_species in species_of_interest:
            indices_of_interest = np.array(
                [
                    index
                    for index, atom
                    in enumerate(self._chemical_symbols)
                    if atom == atomic_species
                ],
                dtype='i'
            )
            # Python to fortran index i -> i+1
            indices_of_interest = indices_of_interest + 1
            nat_of_interest     = len(indices_of_interest)

            log.write(
                '\n    ! Calculating VAF for atomic species {}\n'
                '      Structure contains {} atoms of type {} at indices {}\n'
                '      Max time (fs)     = {}\n'
                '      Max time (dt)     = {}\n'
                '      Stepsize for t    = {}\n'
                '      stepsize for tau  = {}\n'
                '      nr of timesteps   = {}\n'
                '      shape velocities  = {}\n'
                '      Calculating VAF with fortran subroutine fortvaf.calculate_vaf_specific_atoms\n'
                ''.format(
                    atomic_species,
                    nat_of_interest,
                    atomic_species,
                    ','.join([str(i) for i in indices_of_interest]),
                    max_t_vaf_fs,
                    max_t_vaf_dt,
                    stepsize_t_vaf,
                    stepsize_tau_vaf,
                    nr_of_t,
                    velocities.shape
                )
            )


            vaf = fortvaf.calculate_vaf_from_velocities(
                velocities,
                indices_of_interest,
                stepsize_t_vaf,
                stepsize_tau_vaf,
                nr_of_t,
                nstep,
                nat,
                nat_of_interest
            )
            log.write(
                "      calculate_vaf_specific_atoms, returned array of length {}\n"
                "".format(len(vaf))
            )
            self.vaf_all_species.append(vaf)
        self.vaf_results_dict = dict(
            max_t_vaf_fs    =   max_t_vaf_fs,
            max_t_vaf_dt    =   max_t_vaf_dt,
            stepsize_t_vaf  =   stepsize_t_vaf,
            stepsize_tau_vaf=   stepsize_tau_vaf,
            timestep_in_fs  =   timestep_in_fs,
            nr_of_t         =   nr_of_t,
            species_of_interest=species_of_interest,
        )
        return self.vaf_results_dict, np.array(self.vaf_all_species)



    def get_vaf(self, **kwargs):

        raise DeprecationWarning
        self.velocity_autocorrelation_functions = []
        log = self.log

        species_of_interest = self.params['species_of_interest']
        max_t_msd_fs      = self.params['max_t_msd_fs']
        max_ratio           = self.params['max_ratio']
        stepsize            = self.params['stepsize']

        trajectory = self.trajectory
        structure  = self.structure


        total_steps = len(trajectory)
        total_time = len(trajectory)*self.timestep_in_fs
        velocities = min(
            [
                int((total_steps)*max_ratio),
                int(max_t_msd_fs / self.timestep_in_fs)
            ]
        )

        for atomic_species in species_of_interest:
            indices_of_interest = [
                index for index, atom
                in enumerate(structure['atoms'])
                if atom == atomic_species]
            log.write(  '\n    ! Dealing with atomic species {}\n'
                        '      Structure contains {} atoms of type {}\n'
                        ''.format(
                            atomic_species,
                            len(indices_of_interest),
                            atomic_species
                        )
            )
            # Get the trajectory of the ions you are interested in:
            trajectories_of_interest  = np.array(
                zip(
                    *[
                        traj
                        for index, traj
                        in enumerate(zip(*trajectory.tolist()))
                        if index in indices_of_interest
                    ]
                )
            )
            # Get the velocities at each timestep from
            # the difference in position:
            log.write(  '      Calculating velocities... \n')
            velocities_of_interest = np.array(
                [
                    positions - trajectories_of_interest[timestep-1]
                    for timestep, positions
                    in enumerate(trajectories_of_interest[1:])
                ]
            )

            # In zipped velocities, the principical index no longer
            # refers to a timestep, but to the an atom:
            zipped_velocities = [
                np.array(i) for i in zip(*velocities_of_interest)
            ]
            zipped_trajectories = [
                np.array(i) for i in zip(*trajectories_of_interest)
            ]
            range_for_t = range(0,velocities, stepsize)
            log.write(
                '         Done\n'
                '      Calculating velocity autocorrelation function for {} fs ({} timesteps)\n'
                '      Nr of timestep I will calculate is {}\n'
                ''.format(self.timestep_in_fs*velocities, velocities, len(range_for_t))
            )

            # I need the length of the velocity array:
            length_vel = len(velocities_of_interest) # integer of length of velocities

            #~ velocity_autocorrelation_mean = map(get_vaf_of_t, range(max_steps_back))
            number_of_processors = kwargs.get('ncpus', 1)


            #~ def get_mean_msd_of_block(block_start, range_for_t, zipped_trajectories, timestep_in_fs, index_start_vaf_is_0):
            if number_of_processors > 1:
                log.write('    Running with multiprocessing package on {} CPUs\n'.format(number_of_processors))
                from multiprocessing import Pool
                pool = Pool(number_of_processors)

                velocity_autocorrelation_function = pool.map(
                    get_vaf_of_t,
                    itertools.izip(
                        range_for_t,
                        itertools.repeat(zipped_velocities),
                    )
                )
            #~ velocity_autocorrelation_function = [get_vaf_of_t(t, zipped_velocities) for t in  range(velocities)]
            else:
                velocity_autocorrelation_function = map(
                    get_vaf_of_t,
                    itertools.izip(
                        range_for_t,
                        itertools.repeat(zipped_velocities),
                    )
                )
            #~ velocity_autocorrelation_function = [get_vaf_of_t(t, zipped_velocities) for t in  range(velocities)]
            self.velocity_autocorrelation_functions.append(velocity_autocorrelation_function)
        self.length_of_vaf = velocities
        #~ attrs = {
            #~ 'stepsize':stepsize,
            #~ 'timestep_in_fs':self.timestep_in_fs
        #~ }
        return np.array(self.velocity_autocorrelation_functions)



    def get_msd_fort(self, **kwargs):
        """
        Calculates the mean square discplacement (MSD),
        Using the fortran module in lib.fortmsd.

        .. figure:: /images/fort_python_msd.pdf
            :figwidth: 100 %
            :width: 100 %
            :align: center

            Comparison between results achieved with the fortran implementation and python
            showing perfect agreement with respect to each other.

        Velocity autocorrelation function (VAF) and conductivity from the trajectory
        passed in :func:`set_trajectory`
        and the structure set in :func:`set_structure`.
        This procedes in several steps:

        #.  Calculate the MSD for each block, calculate the slope from the VAF0 estimate to block end
        #.  Calculate the mean and the standard deviation of the slope
        #.  Calculate the conductivity, including error propagation.
        """
        from fortmsd import calculate_msd_specific_atoms
        try:
            trajectory = self.trajectory
            timestep_in_fs = self.timestep_in_fs
            total_steps = len(trajectory)
            total_time = total_steps*self.timestep_in_fs
            nstep, nat, _ = trajectory.shape
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_trajectory method to set trajectory"
                "\n\n\n"
            )
        try:
            structure = self.structure
            self._chemical_symbols
        except AttributeError:
            raise Exception(
                "\n\n\n"
                "Please use the set_structure method to set structure"
                "\n\n\n"
            )
        log = self.log # set in __init__
        self.msd_isotrop_all_species = []

        # Getting parameters for calculations
        species_of_interest = kwargs.get('species_of_interest', None)
        if species_of_interest is None:
            species_of_interest =  list(set(self._chemical_symbols))

        stepsize_t_msd  = kwargs.get('stepsize_t_msd', 1)
        vaf_is_0_fs     = kwargs.get('vaf_is_0_fs', None)
        if not isinstance(vaf_is_0_fs, float):
            raise Exception(
                "\n\n\n"
                "Set the time that you consider the\n"
                "VAF to have converged as a float with\n"
                "keyword vaf_is_0_fs"
                "\n\n\n"
            )
        # Setting params for calculation of MSD and conductivity
        # Future: Maybe allow for element specific parameter settings?

        index_start_vaf_is_0 = int(vaf_is_0_fs / (stepsize_t_msd*timestep_in_fs))

        # block params
        block_length_fs = kwargs.get('block_length_fs', None)
        if not isinstance(block_length_fs, float):
            raise Exception(
                "Please set the block length in femtoseconds"
            )
        block_length_dt  = int(block_length_fs / timestep_in_fs)

        # How long to calculate?(maxtime)
        max_t_msd_fs  = kwargs.get('max_t_msd_fs', None)
        if not isinstance(max_t_msd_fs, float):
            raise Exception(
                "\n\n\n"
                "Please set the time to calculate MSD for (in femtoseconds)"
                "\n\n\n"
            )
        max_time_in_dt  = int(max_t_msd_fs / timestep_in_fs)
        nr_of_blocks   = (total_steps -max_time_in_dt) / block_length_dt
        nr_of_t        = max_time_in_dt / stepsize_t_msd

        self.msd_results_dict = {atomic_species: {}
                for atomic_species
                in species_of_interest
            }

        for atomic_species in species_of_interest:

            indices_of_interest = np.array(
                [
                    index
                    for index, atom
                    in enumerate(self._chemical_symbols)
                    if atom == atomic_species
                ],
                dtype='i'
            )
            # Python to fortran index i -> i+1
            if len(indices_of_interest) == 0:
                raise Exception(
                    "empty array when looking for\n"
                    "incides for {}"
                    "".format(atomic_species)
                )
            indices_of_interest  = indices_of_interest + 1
            
            nat_of_interest      = len(indices_of_interest)

            log.write(
                '\n    ! Calculating MSD for atomic species {}\n'
                '      Structure contains {} atoms of type {}\n'
                '      Assuming convergence of VAF at {}\n'
                '      Block length for calculation is {} fs ({} dt)\n'
                '      I will calculate {} blocks\n'
                '      Using fortran subroutine fortmsd.calculate_msd_specific_atoms\n'
                ''.format(
                    atomic_species,
                    nat_of_interest, atomic_species,
                    vaf_is_0_fs,
                    block_length_fs, block_length_dt,
                    nr_of_blocks
                )
            )

            # Get the trajectory of the ions you are interested in:
            #~ trajectories_of_interest  = np.array(zip(*[traj for index, traj
            # in enumerate(zip(*trajectory.tolist())) if index in indices_of_interest]))
            #~ zipped_trajectories = np.array(zip(*trajectories_of_interest))
            #~ print zipped_trajectories.shape


            msd_isotrop_this_species = calculate_msd_specific_atoms(
                trajectory,
                indices_of_interest,
                stepsize_t_msd,
                block_length_dt,
                nr_of_blocks,
                nr_of_t,
                nstep,
                nat,
                nat_of_interest
            )
            

            log.write('      Done, analyzing slopes\n')
            range_for_t = stepsize_t_msd*timestep_in_fs*np.arange(index_start_vaf_is_0, nr_of_t)
            slopes, intercepts, _, _, _ = zip(*[
                    linregress(range_for_t, block[index_start_vaf_is_0:])
                    for block
                    in msd_isotrop_this_species
                ])

            self.msd_results_dict[atomic_species].update({
                'slope_msd_mean':np.mean(slopes),
                'slope_msd_std':np.std(slopes),
                'slope_msd_sem':standard_error_of_mean(slopes),
                'slopes_n_intercepts':zip(slopes,intercepts),
            })



            log.write(
                '      Done, these are the results for {}'
                '\n'.format(atomic_species)
            )
            [
                log.write(  '          {:<20} {}\n'.format(key,  val))
                for key, val in self.msd_results_dict[atomic_species].items()
                if not isinstance(val, (tuple, list, dict))
            ]




            self.msd_isotrop_all_species.append(msd_isotrop_this_species)

        self.msd_results_dict.update({
            'index_start_vaf_is_0'  :   index_start_vaf_is_0,
            'nr_of_blocks'          :   nr_of_blocks,
            'block_length_dt'       :   block_length_dt,
            'stepsize_t_msd'        :   stepsize_t_msd,
            'species_of_interest'   :   species_of_interest,
            'msd_from_start'        :   True,
            'timestep_in_fs'        :   timestep_in_fs,
            'nr_of_t'               :   nr_of_t,
        })

        return self.msd_results_dict, np.array(self.msd_isotrop_all_species)

    def calculate_conductivity(self, temperature):
        self.conductivity_results = {'temperature':temperature}
        for atomic_species in self.msd_results_dict['species_of_interest']:
            print "   calculating diffusion and conductivity for", atomic_species
            (diffusion_mean,
            diffusion_std,
            diffusion_sem,
            conductivity_mean,
            conductivity_std,
            conductivity_sem) = nernst_einstein_eq(
                    self.msd_results_dict[atomic_species]['slope_msd_mean'],
                    self.msd_results_dict[atomic_species]['slope_msd_std'],
                    self.msd_results_dict[atomic_species]['slope_msd_sem'],
                    temperature=temperature,
                    volume=self.structure.get_cell_volume(), # in AA3
                    nr_of_ions=self._chemical_symbols.count(atomic_species)
                )
            resdict = {
                'diffusion_mean_SI':    diffusion_mean,
                'diffusion_std_SI':     diffusion_std,
                'diffusion_sem_SI':     diffusion_sem,
                'conductivity_mean_SI': conductivity_mean,
                'conductivity_std_SI':  conductivity_std,
                'conductivity_sem_SI':  conductivity_sem,
            }
            for k,v in resdict.items():
                print '      {:<20} | {}'.format(k,v)
            self.conductivity_results[atomic_species] = resdict
        return self.conductivity_results

    def set_msd_results(self, res, array=None):
        if array is None:
            self.msd_isotrop_all_species = res.pop('array')
        else:
            self.msd_isotrop_all_species=array
        self.msd_results_dict = res

    def set_vaf_results(self, res, array=None):
        if array is None:
            self.vaf_all_species = res.pop('array')
        else:
            self.vaf_all_species = array
        self.vaf_results_dict = res

    def plot_results(self, **kwargs):
        """
        Plots the results of :func:`calculate_conductivity`

        .. figure:: /images/block_analysis_mp-685863_Li4Si1O4-temp-1000.pdf
            :figwidth: 100 %
            :width: 100 %
            :align: center

            An automatic trajectory analysis for mp-685863 at 1000K.
        """
        try:
            vaf_all_species = np.array(self.vaf_all_species)
            timestep_in_fs  = self.vaf_results_dict['timestep_in_fs']
            plot_vaf        = True
        except AttributeError as e:
            #~ print e
            plot_vaf        = False
        try:
            msd_all_species = self.msd_isotrop_all_species
            timestep_in_fs  = self.msd_results_dict['timestep_in_fs']
            plot_msd        = True
        except AttributeError as e:
            plot_msd        = False
        if not (plot_msd or plot_vaf):
            raise Exception(
                '\n\n'
                "Nothing to plot\n"
                "Run either block analysis of VAF calculation\n"
                "or set the results from previous calculations\n\n"
            )


        if plot_msd and plot_vaf:
            gs = GridSpec(2,1, wspace = 2, hspace = 0.3, height_ratios = [1,2])
            print "HERE"
        else:
            gs = GridSpec(1,1)
        gsindex = 0

        name = kwargs.get('name', False)

        cmap = plt.get_cmap('jet_r')
        fig = plt.figure(figsize = (12, 7))
        plt.suptitle('Analysis of MD-trajectory ({})'.format(name), fontsize=16) # .format(timestep_in_fs / 1000 * total_steps)
        if plot_msd:
            try:
                nr_of_blocks            =  self.msd_results_dict['nr_of_blocks']
                block_length_dt         =  self.msd_results_dict['block_length_dt']
            except KeyError:
                _, nr_of_blocks,block_length_dt = msd_all_species.shape
            try:
                index_start_vaf_is_0    =  self.msd_results_dict['index_start_vaf_is_0']
            except KeyError:
                index_start_vaf_is_0    =  self.msd_results_dict['Li']['index_start_vaf_is_0']
            stepsize_t_msd          =  self.msd_results_dict.get('stepsize_t_msd', 1)

        if plot_vaf:
            axes_vaf = fig.add_subplot(gs[gsindex])
            gsindex += 1
            plt.ylabel(r'VAF $[\left(\frac{\AA}{fs}\right)^2]$')
            plt.xlabel('Time $t$ [fs]')
            if name:
                plt.title('VAF and block size ({})'.format(name))
            else:
                plt.title('VAF and block size')
            times_vaf = timestep_in_fs*self.vaf_results_dict.get(
                    'stepsize_t_vaf', 1
                )*np.arange(self.vaf_results_dict.get(
                    'nr_of_t', vaf_all_species.shape[1]
                ))
            for index_of_species, atomic_species in enumerate(
                    self.vaf_results_dict['species_of_interest']
                ):
                vaf = vaf_all_species[index_of_species]

                if atomic_species == 'H':
                    color = 'blue'
                else:
                    color = jmol_colors[atomic_numbers[atomic_species]]
                axes_vaf.plot(
                        times_vaf, vaf,
                        label='VAF {}'.format(atomic_species),
                        color=color
                    )

            if plot_msd:
                # This will of course crash if you calculate different species in vaf / msd
                #~ conductivity_mean_SI    =  self.msd_results_dict[atomic_species]['conductivity_mean_SI']
                #~ conductivity_std_SI     =  self.msd_results_dict[atomic_species]['conductivity_std_SI']
                #~ conductivity_sem_SI     =  self.msd_results_dict[atomic_species]['conductivity_sem_SI']
                #~ diffusion_mean_SI       =  self.msd_results_dict[atomic_species]['diffusion_mean_SI']
                #~ diffusion_std_SI        =  self.msd_results_dict[atomic_species]['diffusion_std_SI']
                #~ diffusion_sem_SI        =  self.msd_results_dict[atomic_species]['diffusion_sem_SI']

                axes_vaf.axvspan(
                    timestep_in_fs*stepsize_t_msd*index_start_vaf_is_0,
                    timestep_in_fs*block_length_dt,
                    linewidth = 5, alpha=0.2, color=color,
                    label='block size {}: {:.2f} ps '.format(
                            atomic_species,
                            timestep_in_fs / 1000. * block_length_dt
                        )
                )
                #~ axes_vaf.plot(
                    #~ [],
                    #~ color = color,
                    #~ label = r'Conductivity:  ${:.4f} \pm {:.4f} \frac{{S}}{{m}}$'.format(
                        #~ conductivity_mean_SI,
                        #~ conductivity_sem_SI
                    #~ )
                #~ )
                #~ axes_vaf.plot(
                    #~ [],
                    #~ color = color,
                    #~ label = r'Diffusion:  ${:.4g} \pm {:.4g} \frac{{S}}{{m}}$'.format(
                        #~ diffusion_mean_SI,
                        #~ diffusion_sem_SI
                    #~ )
                #~ )
            axes_vaf.legend()

        if plot_msd:
            axes_msd = fig.add_subplot(gs[gsindex])
            gsindex += 1
            plt.ylabel(r'MSD $[\AA^2]$')
            plt.xlabel('Time $t$ [fs]')
            plt.title('Block analysis of MSD')
            if self.msd_results_dict.get('msd_from_start', False):
                times_msd = timestep_in_fs*self.msd_results_dict.get(
                        'stepsize_t_msd',1
                    )*np.arange(
                        self.msd_results_dict.get('nr_of_t', block_length_dt)
                    )
            else:
                #~ print self.msd_results_dict['nr_of_t'], index_start_vaf_is_0
                times_msd = timestep_in_fs*self.msd_results_dict.get(
                        'stepsize_t_msd',1
                    )*np.arange(
                        index_start_vaf_is_0,
                        self.msd_results_dict.get('nr_of_t', block_length_dt+index_start_vaf_is_0)
                    )
            times_fit =  timestep_in_fs*self.msd_results_dict.get(
                    'stepsize_t_msd',1
                )*np.arange(
                    index_start_vaf_is_0,
                    self.msd_results_dict.get('nr_of_t', block_length_dt+index_start_vaf_is_0)
                )

            for index_of_species, atomic_species in enumerate(
                    self.msd_results_dict['species_of_interest']
                ):
                for block_index in range(nr_of_blocks):
                    lower_bound = timestep_in_fs * block_index * block_length_dt
                    slope_this_block, intercept_this_block = self.msd_results_dict[atomic_species]['slopes_n_intercepts'][block_index]
                    msdisotrop = self.msd_isotrop_all_species[index_of_species][block_index]
                    color = cmap(float(block_index)/nr_of_blocks)
                    axes_msd.plot(
                            times_msd, msdisotrop,
                            color=color,
                            label='I {}, lb = {}'.format(block_index, lower_bound)
                        )
                    axes_msd.plot(times_fit,
                            [slope_this_block*x+intercept_this_block for x in times_fit],
                            color=color, linestyle='--'
                        )

        plt.show(block = kwargs.get('block', True))

def get_vaf_of_t(args):
    """
    Get velocity autocorelation function for single time t averaged voer trajectory
    """
    t, velocities_all_atoms = args
    range_of_tau = range(len(velocities_all_atoms[0]) - t)
    values = [
        np.dot(velocities_this_atom[tau+t], velocities_this_atom[tau])
        for tau in range_of_tau
        for velocities_this_atom in velocities_all_atoms
    ]
    return np.mean(values)

def get_index_of_min_sem(velocity_autocorrelation_function, return_sem = False, stepsize_acc = 10):
    """
    Receives

    :param velocity_autocorrelation_function: the vaf as a list or numpy array
    :param return_sem: The boolean whether to return the standard error for each time

    As described above (?)
    we have to find a way to estimate the beginning of uncorrelated motion in a :term:`high-throughput` manner.
    To do this, the following algorithm is a applied:

    #.  Calculate the VAF for the trajectory for a time :math:`t_{max}` large enough (TODO: what is large enough?)
    #.  For each time :math:`t_{min} < t_{max}` calculate:

        .. math::
            {\cal{L}} (t)   &= \\frac{\int_{t}^{t_{max}} \\langle A^2 (t') \\rangle \\text{dt'}}{t_{max} - t}


    .. figure:: /images/estimate_beginning_uncorrelated_motion.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center

    .. figure:: /images/estimating_vaf_is_0_dependance.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center

    """
    length_of_vaf = len(velocity_autocorrelation_function)
    list_of_sem, t_min = zip(*[
                (
                    standard_error_of_mean(velocity_autocorrelation_function[starttime:]),
                    starttime
                )
                for starttime in range(0, int(0.8*length_of_vaf), stepsize_acc)
                ])
    index_of_min_sem = np.array(list_of_sem).argmin()
    if return_sem:
        return t_min[index_of_min_sem], list_of_sem
    return t_min[index_of_min_sem]

def nernst_einstein_eq(
        slope_msd_mean, slope_msd_std, slope_msd_sem, 
        temperature, nr_of_ions, volume, charge=1):
    """
    Calculate ionic conducitivity from the Nernst Einstein Equation :math:`\\bar{\\sigma} = \\frac{N e^2}{k_B T} \\bar{D}`
    Receives

    :param slope_msd_mean: The slope of the msd displacement calculate in :func:`TrajectoryAnalyzer.calculate_conductivity` in :math:`\\frac{\\AA^2}{fs}`
    :param slope_msd_std: The error of the slope of the msd displacement estimated in :func:`TrajectoryAnalyzer.calculate_conductivity` doing the block analysis
    :param slope_msd_sem: The standard error of the mean of the slope estimated in :func:`TrajectoryAnalyzer.calculate_conductivity` doing the block analysis
    :param structure: Structure in standard dictionary form (see :func:`~carlo.codes.voronoi.structure_imports.structure_wizard`) to get volume and atomic density.
    :param temperature: Temperature of thermostat in K
    :param charge: charge of the conducting ion in units of *e*, defaults to 1

    Returns

    :param diffusion_mean: Mean diffusion coefficient in SI-units
    :param diffusion_std:  Standard error of the diffusion coefficient
    :param diffusion_sem: Standard error of the mean of the diffusion coefficient
    :param conductivity_mean: Mean conductivity
    :param conductivity_std: Standard deviation of the conductivity
    :param conductivity_sem: Standard error of the mean of the conductivity

    The diffusion coefficient is calculated using the Einstein-Smulochowski equation :math:`D = \\frac{1}{6} \\overline{\\frac{d\\langle  \\text{MSD}(t)\\rangle}{\\text{dt}}}`
    This diffusion is inserted into the Nernst-Einstein equation above to obtain the conductivity.
    We can see the results in the following (plotted with :func:`carlo.scripts.plot_all_conductivities.plot_conductivities`

    .. figure:: /images/diffusion_coeffs_n_conductivities_top10_1.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center


    """

    

    density             =   nr_of_ions / volume   # Nr of ions per cubic meter
    # the slope is in A**2 / fs, make that SI-units (m**2/s) 1e-20 / 1e-15
    diffusion_mean      =   slope_msd_mean *  1e-5 / 6.
    diffusion_std       =   slope_msd_std  *  1e-5 / 6.
    diffusion_sem       =   slope_msd_sem  *  1e-5 / 6.
    prefactor           =   charge**2 * e2*density/(kB * temperature)
    conductivity_mean   =   prefactor * diffusion_mean
    conductivity_std    =   prefactor * diffusion_std
    conductivity_sem    =   prefactor * diffusion_sem
    return diffusion_mean, diffusion_std,diffusion_sem, conductivity_mean, conductivity_std, conductivity_sem



def trajectory_assessment(caller, *args, **kwargs):
    def maxwell_boltzmann(vel, mass):
        #~ print np.exp( - exp_factor* mass *vel**2 / T)
        #~ vel = vel_scaling * vel
        #~ alpha = atomic_mass * mass / (2* kB * T)
        #~ return  (alpha / np.pi )**(1.5) * 4* np.pi *  vel**2 * np.exp( - alpha *vel**2)
        return (prefactor * mass / T )**(1.5) * 4* np.pi *  vel**2 * np.exp( - exp_factor* mass *vel**2 / T)
    nr_of_bins = 100
    chunksize = 100
    traj =  args[0]['traj']['array']
    timestep_fs = args[0]['traj']['timestep_in_fs']
    struc =  args[1]['struc']

    atomic_mass = 1.66e-27
    kB = 1.3806488e-23
    vel_scaling = 1e5 / timestep_fs
    prefactor = atomic_mass / (2*np.pi*kB)
    exp_factor = atomic_mass  / (2 * kB)
    T = 600

    nr_of_atoms = traj.shape[1]
    trajlen = len(traj)
    velocities = [[vel_scaling*np.linalg.norm(traj[timestep][atomindex] - traj[timestep-1][atomindex])  for timestep in range(1, trajlen)] for atomindex in range(nr_of_atoms)]
    vel_dict = {atom:{'mass':atomic_masses[atomic_numbers[atom]], 'velocities':[], 'maxbol' : []} for atom in set(struc['atoms'])}
    [vel_dict[struc['atoms'][index]]['velocities'].append(vellist) for index,vellist in enumerate(velocities)]
    for key, d in vel_dict.items():
        d['velocities'] = zip(*d['velocities'])

    fig = plt.figure(figsize = (21,16))
    ax1 = fig.add_subplot(2,1,1)
    for key, d in vel_dict.items():
        temps = [np.mean([atomic_mass * d['mass'] * vel**2 / (3*kB) for vel in timestep]) for timestep in d['velocities']]
        ax1.plot(temps, label = key)

    plt.ylabel('Temperature')
    plt.xlabel('Timestep')
    plt.legend()

    ax2 = fig.add_subplot(2,1,2)

    for key, d in vel_dict.items():
        toplot = []
        for i in range(0, len(traj), chunksize):
            #~ fig = plt.figure()
            #~ plt.title(' Chunk: {}, Element {}'.format(int(i/chunksize), key))
            if chunksize > len(traj) -i: continue
            vels = flatten(d['velocities'][i:i+chunksize])
            x = np.arange(0, max(vels),max(vels)/nr_of_bins)

            counts, bins = np.histogram(vels, bins=nr_of_bins, range=(0,max(vels)), normed=True)
            half_bins = [(bins[jj] + bins[jj+1] ) /2.0  for jj in range(nr_of_bins)]
            boltzmann = [maxwell_boltzmann(vel, d['mass']) for vel in half_bins]
            #~ diffs = np.sum([(count - boltz)**2 for count, boltz in zip(counts, boltzmann)])
            diffs = np.std([count - boltz for count, boltz in zip(counts, boltzmann)])
            toplot.append([i, diffs])
        ax2.plot(*zip(*toplot), label = key)
    plt.xlabel('Timestep')
    plt.ylabel('Deviation from MB')
    plt.legend()
    plt.show()


def get_fixed_reference_msd(trajectory, structure, timestep_in_fs =  1, reference = 0):
    """
    Calculates the MSD with a fixed reference point
    (no running average)
    """
    traj = trajectory
    trajlen = len(traj)
    #~ tracklist = sorted([item for sublist in self.track_dict.values() for item in sublist])
    if type(reference) == int:
        initial_pos = traj[reference]
    elif reference == 'inputstructure':
        initial_pos = structure['positions']
    else: raise Exception('No valid reference point specified')

    msdcoeffs = [
        [
            [
                [
                    (timestep[j][k]-initial_pos[j][k])*(timestep[j][l]-initial_pos[j][l])
                    for k in range(3)
                ]
                for l in range(3)
            ]
            for j, position in enumerate(timestep)
        ]
        for timestep in trajectory
    ]
    msdisotrop = [
        [
            np.linalg.norm(initial_pos[j] - position)**2
            for j, position
            in enumerate(timestep)
        ]
        for timestep in trajectory
    ]


    msd_dict = {atom:{'msd':[]} for atom in set(structure['atoms'])}
    [
        msd_dict[atom]['msd'].append(zip(*msdisotrop)[index])
        for index, atom
        in enumerate(structure['atoms'])
    ]

    for atom, atomdict in msd_dict.items():
        atomdict['atom_mean'] = [np.mean(msd_at_timestep) for msd_at_timestep in  zip(*msd_dict[atom]['msd'])]


    plt.figure(figsize = (16,9)) #clear figures from before
    plt.xlabel('Time [fs]')
    plt.ylabel(r'MSD [$\AA^2$]')
    plt.title(r'MSD in ${}$'.format(''.join(['{}_{{{}}}'.format(atom, structure['atoms'].count(atom)) for atom in set(structure['atoms'])])) )
    times = [timestep_in_fs*i for i in range(len(trajectory))]
    for element, msd in msd_dict.items():
        plt.plot(times, msd['atom_mean'], linewidth = 4,color = jmol_colors[atomic_numbers[element]], label = 'MSD {}'.format( element))
        for msd_one_atom in msd['msd']:
            plt.plot(times, msd_one_atom, linestyle = '--', color = jmol_colors[atomic_numbers[element]], label = 'MSD {}'.format( element))

        #~ fit = self.diffusion_dict[element]
        #~ if len(element)> 3: continue #these are other keys in dictionary, like pdf file path
        #~ points = [fit['D']['slope']*x+fit['D']['intercept'] for x in range(self.equilibration_steps, len(self.traj))]
        #~ times = [1e15*self.dt*x for x in range(self.equilibration_steps, len(self.traj))]
        #~ plt.plot(times, points, linestyle = '--', color = jmol_colors[atomic_numbers[element]], label = 'Lin. Fit {}'.format(element))
    plt.show()
    return
    for key, indices in self.track_dict.items():
        msd_dict[key]  = {}
        diffusion_matrix = np.zeros((3,3))
        for coeff, i,j in [('D_xx',0,0), ('D_xy', 0,1), ('D_xz', 0,2), ('D_yy', 1,1), ('D_yz', 1,2), ('D_zz', 2,2)]:
            msd_dict[key][coeff] =  [np.mean([(timestep[index][i]-initial_pos[index][i])*(timestep[index][j]-initial_pos[index][j]) for index in indices]) for timestep in traj]
        msd_dict[key]['D'] = [np.mean([np.linalg.norm(initial_pos[index] - timestep[index])**2 for index in indices]) for timestep in traj]
    self.log.write('   Computing MSD and writing results to {}\n'.format(msd_file_path))
    print 11
    json.dump(msd_dict, open(msd_file_path, 'w'))
    print 12
    print 1
    if os.path.exists(res_file_path) and not self.overwrite:
        print 2
        diffusion_dict = json.load(open(res_file_path))
        self.log.write('   Loading linear fits from {}\n'.format(res_file_path))
    else:
        print 3
        self.log.write('   Computing linear fits and storing results in {}\n'.format(res_file_path))
        diffusion_dict = {}
        for element, msd_element_dict in msd_dict.items():
            #~ print element
            diffusion_dict[element] = {}
            for coefficient_key, msd in msd_element_dict.items():
                diffusion_dict[element][coefficient_key] = {
                    key:val
                    for key, val in zip(
                        ['slope', 'intercept', 'r-value', 'p-value', 'stderr'],
                        linregress(
                            range(self.equilibration_steps, trajlen),
                            msd[self.equilibration_steps:]
                        )
                    )
                }
                if coefficient_key == 'D':
                    diffusion_dict[element][coefficient_key]['value'] = 1e-20 / self.dt * diffusion_dict[element][coefficient_key]['slope'] / 6
                else:
                    diffusion_dict[element][coefficient_key]['value'] = 1e-20 / self.dt * diffusion_dict[element][coefficient_key]['slope'] / 2
            diffusion_dict[element]['diffusion_matrix'] = [
                [
                    diffusion_dict[element]['D_'+''.join(sorted([dir1,dir2]))]['value']
                    for dir1 in ('x','y','z')
                ]
                for dir2 in ('x','y','z')
            ]

        cell = self.struc['cell']
        volume = np.dot(cell[0], np.cross(cell[1], cell[2]))*10**(-30)  #cell volume in cubic meters
        #~ print volume
        #~ print Atoms(cell = cell).get_volume() #just a test
        nr_of_ions = self.struc['atoms'].count('Li')
        e = 1.60217657*10**(-19)
        e2 = e**2
        #calculating the conductivity sigma of Li
        for element in diffusion_dict.keys():
            nr_of_ions = self.struc['atoms'].count(element)
            diff = np.array(diffusion_dict[element]['diffusion_matrix'])
            sigma = e2*diff*nr_of_ions/(volume*1.3806488*10**(-23) * self.temperature)
            diffusion_dict[element]['sigma']  =  sigma.tolist()
            diffusion_dict[element]['sigma_slope']  =  e2*diffusion_dict[element]['D']['value']*nr_of_ions/(volume*1.3806488*10**(-23) * self.temperature)
        self.log.write('\n   conductivity for Li is {} S/m\n'.format(diffusion_dict['Li']['sigma_slope']))
        json.dump(diffusion_dict, open(res_file_path, 'w'))
    self.diffusion_dict = diffusion_dict
    self.msd_dict = msd_dict


def get_rdf(self, filename, nr_of_bins=200, nr_of_timesteps = False, timelags =  range(0, 201, 20)):
    """
    Calculate the radial distribution function in a simulation"""

    def get_distance(point1, point2):
        point1 =   collapse_into_unit_cell(point1, self.cell)
        point2 =   collapse_into_unit_cell(point2, self.cell)
        return find_closest_periodic_image(point2, point1, self.cell)[0]
    def get_density(distance, counts):
        #print counts, volume, nr_of_timesteps, nr_of_lithiums, nr_of_atoms, distance,  binwidth
        return counts * volume / (nr_of_timesteps * nr_of_lithiums* nr_of_atoms * 4*np.pi * distance**2 *binwidth)
        #~ return counts * volume / (nr_of_timesteps * nr_of_lithiums* nr_of_atoms * 4.0  / 3.0*np.pi * ((distance+binwidth)**3 -distance**3))

    self.timelags_dt = [int(float(timelag)/self.timestep_in_fs) for timelag in timelags] #timelag in fs ->timelag in timesteps
    self.timelags_fs = timelags
    #~ print self.timelags_dt
    #~ print self.timelags_fs
    maxcorrect = 0.5*min([np.linalg.norm(latticevector) for latticevector in self.struc['cell']]) #this is how far you can expand the radius if you still want correct result
    binwidth = float(maxcorrect)/nr_of_bins
    nr_of_lithiums = self.struc['atoms'].count('Li')
    cell = self.struc['cell']
    volume = np.dot(cell[0], np.cross(cell[1], cell[2]))  #cell volume in cubic meters
    #~ end = len(self.traj)
    start  = self.equilibration_steps

    if nr_of_timesteps:
        end = nr_of_timesteps +start
    else:
        #~ start = self.equilibration_steps
        #~ nr_of_timesteps = end - start
        end =  min([start+int(3000.0/self.timestep_in_fs) , len(self.traj)])   #default take first 3ps after equilibration
        nr_of_timesteps = end-start
    print start, end
    rdf_dict = {}
    coordination_dict = {}
    #~ print range(start, end)
    self.log.write('   Calculating RDF for elements {} with respect to Li\n'.format(','.join(self.track_dict.keys())))


    for element in self.track_dict.keys():
        nr_of_atoms = self.struc['atoms'].count(element)
        #~ print element, nr_of_atoms
        fil = filename.replace('.npy', '_{}.npy'.format(element))
        if os.path.exists(fil) and not self.overwrite:
            self.log.write('      Reading distances from {} \n'.format(fil))
            rdf_dict[element] = np.load(fil)
        else:
            self.log.write('      Calculating distances for {} \n'.format(element))
            #~ distances = [[np.linalg.norm(np.array(collapse_into_unit_cell(self.traj[timestep-timelag][li_index], self.cell)) - collapse_into_unit_cell(self.traj[timestep][el_index], self.cell)) \
                            #~ for li_index in self.track_dict['Li']  \
                            #~ for el_index in self.track_dict[element] \
                            #~ for timestep in range(self.equilibration_steps, len(self.traj)) \
                            #~ if li_index != el_index] for timelag in self.timelags]
            distances = [[get_distance(self.traj[timestep-timelag][li_index], self.traj[timestep][el_index]) \
                            for li_index in self.track_dict['Li']  \
                            for el_index in self.track_dict[element] \
                            for timestep in range(start, end) \
                            if li_index != el_index] \
                            for timelag in self.timelags_dt]
            #~ histos  = [np.histogram(distance, bins =10000, normed =nr_of_atoms) for distance in distances]
            histos  = [np.histogram(distance, bins = nr_of_bins, range = (0, maxcorrect)) for distance in distances]
            rdfs = [[(distance, get_density(distance, counts)) for counts, distance in zip(*histo)[1:]] for histo in histos]
            #~ n_ = [[(distance, np.sum(rdf[:distance_index])) for distance_index in range(nr_of_bins)] for distance,
            self.log.write('      Saving histogram to {} \n'.format(fil))
            np.save(fil, rdfs)
            rdf_dict[element] = rdfs

    self.rdf_dict = rdf_dict

def get_mean_msd_of_block(args):
    block_start, range_for_t, zipped_trajectories, timestep_in_fs, index_start_vaf_is_0, block_length = args
    msd_isotrop_this_block = [
        np.mean(
            [
                np.linalg.norm(trajectory_of_ion[tau + t] - trajectory_of_ion[tau])**2
                for tau in np.arange(block_start, block_start + block_length)
                for trajectory_of_ion in zipped_trajectories
            ]
        )
        for t in range_for_t
    ]

    slope_this_block, intercept_this_block, r_value, p_value, std_err = linregress(
        timestep_in_fs * range_for_t[index_start_vaf_is_0:],
        msd_isotrop_this_block[index_start_vaf_is_0:]
    )
    return slope_this_block, intercept_this_block, msd_isotrop_this_block




def calculate_msd_normalizing_etc():
    """
    get the time averaged trajectory, depracated and replaced by calculate_conductivity
    """
    raise Exception("deprecated")
    trajectory = self.trajectory
    log = self.log
    structure = self.structure
    total_steps = len(trajectory)
    total_time = len(trajectory)*self.timestep_in_fs
    nr_of_atoms = len(structure['atoms'])
    max_ratio = 0.9 #Go back half the simulation time without the equilibration time
    max_time = 10 # The maximum time in fs that you want to run the running average for
    equilibration_time =  100.0   # equilibration time in femtoseconds
    equilibration_steps = int(equilibration_time / self.timestep_in_fs)

    #~ max_steps_back = min([int((total_steps - equilibration_steps)*max_ratio), int(max_time*self.timestep_in_fs)])
    max_steps_back = int((total_steps - equilibration_steps)*max_ratio)



    log.write(  '   I got a trajectrory of shape {} and a structure of {} atoms\n'.format(trajectory.shape, len(structure['atoms'])))
    log.write(  '      Equilibration time set to {} fs ({} timesteps) \n'.format(equilibration_time, equilibration_steps))
    log.write(  '      I will take {} steps back...\n'
                '      Minimal sample size: {} \n'.format(max_steps_back, total_steps - equilibration_steps - max_steps_back))
    log.write(  '      Calculating velocities... \n')
    velocities = [  positions - trajectory[timestep-1]
                    for timestep, positions in enumerate(trajectory[1:])
                    if timestep > equilibration_steps
                ]

    log.write(  '         Done\n'
                '      Calculating normalized velocities... \n')


    average_velocities_per_species = {
            atom_type:np.array([
            [np.mean(d) for d in zip(*np.array([vel for index, vel in enumerate(velocities_this_timestep) if structure['atoms'][index] == atom_type]).tolist())]
            for velocities_this_timestep in velocities
            ])
            for atom_type in set(structure['atoms'])}

    normalized_velocities  = [
            [
                velocity - average_velocities_per_species[structure['atoms'][atomindex]][timestep_index]
                for atomindex, velocity in enumerate(velocities_this_timestep)
            ]
        for timestep_index, velocities_this_timestep in enumerate(velocities)]

    log.write(  '         Done\n')
    log.write(  '      Calculating running averaged MSD... \n')
    msdisotrop = [[np.mean([np.linalg.norm(trajectory[timestep_now - timesteps_back][j] - trajectory[timestep_now][j])**2
                            for timestep_now in range(equilibration_steps+timesteps_back, total_steps)])
                                for timesteps_back in range(max_steps_back)]
                                    for j in range(nr_of_atoms)]

    log.write(  '         Done\n'
                '      Calculating velocity autocorrelation function\n')

    velocity_autocorrelation = \
        [[np.mean([np.dot(velocities[timestep_now][j], velocities[timestep_now - timesteps_back][j])
            for timestep_now in range(timesteps_back, len(velocities))])
                for timesteps_back in range(max_steps_back)]
                    for j in range(nr_of_atoms)]
    log.write(  '         Done\n'
                '      Calculating velocity autocorrelation function of mean velocities\n')
    mean_velocity_autocorrelation = {
        atom_type: [np.mean([np.dot(mean_velocities[timestep_now], mean_velocities[timestep_now - timesteps_back])
            for timestep_now in range(timesteps_back, len(velocities))])
                for timesteps_back in range(max_steps_back)]
            for atom_type, mean_velocities in average_velocities_per_species.items()
        }
    log.write(  '         Done\n'
                '      Calculating normalized velocity autocorrelation function\n')

    normalized_velocity_autocorrelation = \
        [[np.mean([np.dot(normalized_velocities[timestep_now][j], normalized_velocities[timestep_now - timesteps_back][j])
            for timestep_now in range(timesteps_back, len(normalized_velocities))])
                for timesteps_back in range(max_steps_back)]
                    for j in range(nr_of_atoms)]
    log.write(  '         Done\n'
                '      Calculating means...\n')

    results_dict = {atom:{'msd':[], 'vel_corr':[], 'norm_vel_corr':[]} for atom in set(structure['atoms'])}

    [results_dict[atom]['msd'].append(msdisotrop[index]) for index, atom in enumerate(structure['atoms'])]
    [results_dict[atom]['vel_corr'].append(velocity_autocorrelation[index]) for index, atom in enumerate(structure['atoms'])]
    [results_dict[atom]['norm_vel_corr'].append(normalized_velocity_autocorrelation[index]) for index, atom in enumerate(structure['atoms'])]
    for atom, atomdict in results_dict.items():
        atomdict['mean_msd'] = [np.mean(msd_at_timestep) for msd_at_timestep in  zip(*results_dict[atom]['msd'])]
        atomdict['mean_vel_corr'] = [np.mean(vel_corr_at_timestep) for vel_corr_at_timestep in  zip(*results_dict[atom]['vel_corr'])]
        atomdict['mean_norm_vel_corr'] = [np.mean(norm_vel_corr_at_timestep) for norm_vel_corr_at_timestep in  zip(*results_dict[atom]['norm_vel_corr'])]
        atomdict['corr_of_mean_vel'] = list(mean_velocity_autocorrelation[atom])
        atomdict['correlation_difference'] = list(np.array(atomdict['mean_vel_corr']) - structure['atoms'].count(atom)* np.array(mean_velocity_autocorrelation[atom]))

    log.write(  '         Done\n')
    self.results_dict = results_dict
    return self.results_dict
def plot_results_of_conductivity_calc_without_vaf(self):

    """
    Plots the results of :func:`calculate_conductivity`

    .. figure:: /images/block_analysis_mp-685863_Li4Si1O4-temp-1000.pdf
        :figwidth: 100 %
        :width: 100 %
        :align: center

        An automatic trajectory analysis for mp-685863 at 1000K.
    """
    raise DeprecationWarning
    # total_steps = self.results_dict['total_steps']
    timestep_in_fs = self.timestep_in_fs
    stepsize   = self.params['stepsize']
    # velocities = self.results_dict['velocities']
    cmap = plt.get_cmap('jet_r')
    # first plot
    fig = plt.figure(figsize = (20, 12))
    plt.suptitle('Analysis of MD-trajectory', fontsize =  16) # .format(timestep_in_fs / 1000 * total_steps)
    ax2 = fig.add_subplot(1,1,1)
    plt.ylabel(r'MSD $[\AA^2]$')
    plt.xlabel('Time $t$ [fs]')
    plt.title('Block analysis of MSD')
    #~ print
    #~ times = timestep_in_fs*np.arange(self.length_of_vaf)

    for index_of_species, atomic_species in enumerate(self.results_dict['species_of_interest']):
        index_start_vaf_is_0 = self.results_dict[atomic_species]['index_start_vaf_is_0']
        #~ length_of_vaf = self.results_dict[atomic_species]['length_of_vaf']
        block_length =  self.results_dict[atomic_species]['block_length']
        nr_of_blocks =  self.results_dict[atomic_species]['nr_of_blocks']
        conductivity_mean_SI =  self.results_dict[atomic_species]['conductivity_mean_SI']
        conductivity_std_SI =  self.results_dict[atomic_species]['conductivity_std_SI']
        conductivity_sem_SI =  self.results_dict[atomic_species]['conductivity_sem_SI']
        diffusion_mean_SI =  self.results_dict[atomic_species]['diffusion_mean_SI']
        diffusion_std_SI =  self.results_dict[atomic_species]['diffusion_std_SI']
        diffusion_sem_SI =  self.results_dict[atomic_species]['diffusion_sem_SI']

        #~ velocity_autocorrelation_function = self.velocity_autocorrelation_functions[index_of_species]

        color = jmol_colors[atomic_numbers[atomic_species]]
        #~ ax1.plot(times, velocity_autocorrelation_function, label = 'VAF {}'.format(atomic_species), color = color)
        #~ ax1.axvline(timestep_in_fs*index_start_vaf_is_0, label = 't_min {}'.format(atomic_species), color = color, linewidth = 2.3)
        #~ plt.axvline(self.timestep_in_fs*(index_start_vaf_is_0+block_length), label = 'Block length')
        #~ ax1.axvspan(timestep_in_fs*(index_start_vaf_is_0), timestep_in_fs*(index_start_vaf_is_0+block_length),
                #~ linewidth = 5,
                #~ alpha=0.2,
                #~ color = color,
                #~ label = 'block size {}: {:.2f} ps '.format(atomic_species, timestep_in_fs / 1000. * block_length))
        ax2.plot(
            [],
            color = color,
            label = r'Conductivity:  ${:.4f} \pm {:.4f} \frac{{S}}{{m}}$'.format(
                conductivity_mean_SI,
                conductivity_sem_SI
            )
        )
        ax2.plot(
            [],
            color = color,
            label = r'Diffusion:  ${:.4g} \pm {:.4g} \frac{{S}}{{m}}$'.format(
                diffusion_mean_SI,
                diffusion_sem_SI
            )
        )
        # Second plot
        if self.params['msd_from_start']:
            times = timestep_in_fs*np.arange(0, block_length, self.params['stepsize'])
        else:
            times = timestep_in_fs*np.arange(index_start_vaf_is_0, block_length, self.params['stepsize'])
        times_fit = timestep_in_fs*np.arange(index_start_vaf_is_0, block_length, self.params['stepsize'])

        #~ times_fit = timestep_in_fs*np.arange(index_start_vaf_is_0, block_length, stepsize)
        #~ times = timestep_in_fs*np.arange(index_start_vaf_is_0, block_length, stepsize)
        for block_index in range(nr_of_blocks):
            slope_this_block, intercept_this_block = self.results_dict[atomic_species]['slopes_n_intercepts'][block_index]
            msdisotrop = self.msd_isotrop_all_species[index_of_species][block_index]
            #~ color = cmap(float(index)/nr_of_blocks)
            ax2.scatter(
                times,
                msdisotrop,
                color = color,
                s = 1)
            ax2.plot(
                times_fit,
                [slope_this_block*x+intercept_this_block for x in times_fit],
                color = color,
                linestyle = '--'
            )

    ax2.legend()
    plt.show()



if __name__ == '__main__':
    from datetime import datetime
    from time import sleep
    from carlo.orm.model import *
    from carlo.orm.querybuilder import QueryBuilder

    from carlo.plugins.md import conductivity_master, StructureData, TrajectoryData
    from carlo.plugins.quantumespresso import PwImmigrant


    qb = QueryBuilder(
            path=[StructureData, PwImmigrant, TrajectoryData],
            project={
                StructureData:'*',
                TrajectoryData:'*',
            }
        )
    struc, traj = qb.first()
    
    conductivity_master(struc, traj)
    
    sys.exit()
    #~ help(fortmsd)
    #~ print fortmsd.calculate_msd_single_species.__doc__
    #~ sys.exit()
    try_fort = True
    use_flipper = True

    if use_flipper:
        calc = session.query(FlipperCondCalcRev).filter(FlipperCondCalcRev.id == 142246).first()
        struc = calc.inputs_q.filter(Calc.type == 'StructureData').one()
        traj  = calc.inputs_q.filter(Calc.type == 'FlipperTraj').one()
        params = dict(
            block_length_fs     = 10000.0,
            max_t_msd_fs      = 10000.,
            max_t_vaf_fs      = 3000.,
            stepsize_t_vaf     = 2,
            stepsize_t_msd     = 10,
            stepsize_tau_vaf   = 10,
            species_of_interest = ['Li'],
            vaf_is_0_fs         = 500.
        )



    else:
        calc = session.query(CondCalc).filter(CondCalc.id == 117643).first()
        struc = calc.inputs_q.filter(Calc.type == 'StructureData').one()
        traj  = calc.inputs_q.filter(Calc.type == 'WFTraj').one()
        params =  dict(
            block_length_fs = 1500.0,
            max_t_msd_fs      = 1500.,
            stepsize           = 1,
            vaf_is_0_fs         = 500.,
            #~ species_of_interest= None,
        )
    print "   THIS IS calculate_conductivity"
    trajanal =  TrajectoryAnalyzer()
    print "     settings trajectory"
    trajanal.set_trajectory(traj.content)
    print "     settings structure"

    t2 = TrajectoryAnalyzer()
    #~ t2.set_structure(struc.content)

    trajanal.set_structure(struc.content)

    resdict, msdarray = trajanal.calculate_conductivity_fort(**params)
    t2.set_msd_results(resdict, msdarray)
    vafdict, vafarray = trajanal.calculate_vaf_fort(**params)
    t2.set_vaf_results(vafdict, vafarray)


    t2.plot_results()
    #~ print resdict['Li']['conductivity_mean_SI']  # should be 40.377992060620372
    #~ trajanal.plot_results_of_conductivity_calc_without_vaf()

