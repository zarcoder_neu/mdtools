# -*- coding: utf-8 -*-
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"



from aiida.orm.calculation.inline import make_inline
from mdtools.libs.struclib.supercells import make_supercell_aiida


@make_inline
def make_supercell_inline(structure, parameters):
    """
    """
    minimal_dimension = float(parameters.dict.minimal_dimension)
    rattle_sigma=parameters.get_dict().get('rattle_sigma', None)
    if rattle_sigma is not None:
        rattle_sigma = float(rattle_sigma)
    supercell, reps = make_supercell_aiida(
            structure,
            minimal_dimension=minimal_dimension,
            rattle_sigma=rattle_sigma
        )
    return dict(supercell=supercell)
