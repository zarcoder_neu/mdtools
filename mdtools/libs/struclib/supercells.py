# -*- coding: utf-8 -*-
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"
import numpy as np
from math import ceil
from ase import Atoms
from aiida.common.exceptions import InputValidationError

def make_supercell_ase(atoms, minimal_dimension, rattle_sigma=None):
    """
    Repeat a structure such that it satisfies the minimal dimension specified
    in the parameters.

    :param atoms:
        A representation of a structure. Has to be an instance of ase.Atoms
    :param float minimal_dimension:
        The minimum (in Angstrom) for each dimension.
        If you specified 15.0, the cell will be replicated such that the minimum
        distance between boundaries larger than 15.0
    :param float rattle_sigma:
        Optional argument, rattles the unit cells using ase.Atoms.rattle(std=rattle_sigma)

    :returns: the supercell as an instance of ase.Atoms
    :returns: the repetitions applied as list of 3 integers

    """
    try:
        minimal_dimension = float(minimal_dimension)
    except Exception as e:
        raise InputValidationError(
            "The value provided for minimal_dimension ({}) is not valid\n"
            "Exception raised: {}\n"
            "".format(minimal_dimension, e)
        )

    if rattle_sigma is not None:
        try:
            rattle_sigma = float(rattle_sigma)
        except Exception as e:
            raise InputValidationError(
                "The value provided for rattle_sigma ({}) is not valid\n"
                "Exception raised: {}\n"
                "".format(rattle_sigma, e)
            )



    a,b,c = [np.array(v) for v in atoms.cell]
    repetitions =[None]*3
    for index, item in enumerate(((a,b,c), (b,c,a), (c,a,b))):
        n1, n2, n3 = item
        d23 = np.cross(n2, n3) 
        d1 = np.linalg.norm(np.dot(n1, d23)) / np.linalg.norm(d23)
        repetitions[index] = int(ceil(minimal_dimension/d1))
    supercell_atoms = atoms.repeat(repetitions)
    if rattle_sigma is not None:
        supercell_atoms.rattle(rattle_sigma, seed=np.random.randint(0, 100))
    return supercell_atoms, repetitions


def make_supercell_aiida(structure, minimal_dimension, rattle_sigma=None):
    """
    Take an AiiDA structure and replicate it.
    """
    from aiida.orm.data.structure import StructureData
    atoms = structure.get_ase()
    supercell_atoms, repetitions = make_supercell_ase(
            atoms,
            minimal_dimension=minimal_dimension,
            rattle_sigma=rattle_sigma
        )


    supercell_aiida = StructureData()

    if rattle_sigma:
        supercell_aiida._set_attr('rattled', True)
        supercell_aiida._set_attr('rattle_sigma', rattle_sigma)
    else:
        supercell_aiida._set_attr('rattled', False)
    supercell_aiida.set_ase(supercell_atoms)
    supercell_aiida._set_attr('repetitions', repetitions)
    supercell_aiida._set_attr('is_supercell', True)
    supercell_aiida._set_attr('pbc1', True)
    supercell_aiida._set_attr('is_supercell', True)
    supercell_aiida._set_attr('is_supercell', True)

    supercell_aiida.label = structure.get_formula(mode='count')
    return supercell_aiida, repetitions

def make_supercell_selection(structures):
    """
    If there are several representations of a structure,
    take the one whose replication produces the lowest number of atoms:
    """
    def get_supercells_unknown(s, **kwargs):
        if isinstance(s, Atoms):
            supercell, _ = make_supercell_ase(s, **kwargs)
        else:
            
            supercell = make_supercell_aiida(s, **kwargs)
        return supercell
    def get_length(struc):
        """
        Get the length of a structure
        """
        if isinstance(struc, Atoms):
            return len(struc)
        else:
            # Here I am assuming this is an Aiida StructureData instance.
            # I can't check because I'd need to import StructureData,
            # which only works if load_dbenv has been called
            return len(struc.sites)
    

    candidates = [
            get_supercells_unknown(
                    s,
                    minimal_dimension=minimal_dimension,
                    rattle_sigma=rattle_sigma
            )
            for s
            in structures
        ]
    lengths_n_candidates = [
            (get_length(s), reps, s)
            for reps, s
            in candidates
        ]
    nat, repetitions, chosen_atoms = sorted(candidates)[0]




if __name__ == '__main__':
    import sys
    from argparse import ArgumentParser
    from ase.io import read
    from ase.visualize import view
    parser = ArgumentParser()
    parser.add_argument(
            '-m', '--minimal-dimension', default=10.,
            type=float, help='Minimum dimension of supercell'
        )
    parser.add_argument(
            '-r', '--rattle-sigma', type=float, default=None,
            help='Standard deviation of atomic displacement when rattling'
        )
    parsed_args = parser.parse_args(sys.argv[1:])
    pos_GaTaO3 = """
Ga      0.00496714      -0.00138264       0.00647689
Ta      1.99196588       1.97439405       1.97439421
O      1.99252771       1.98440993      -0.00469474
O      1.98216118      -0.00463418       1.97207828
O      0.00241962       1.95760278       1.95948640
"""
    symbols, positions = [], []
    for line in pos_GaTaO3.split('\n')[2:-1]:
        symbol, x, y, z = line.split()[:4]
        symbol = symbol.lower().capitalize()
        symbols.append(symbol)
        positions.append([float(x), float(y), float(z)])
    atoms = Atoms(
            symbols=['Ga', 'Ta', 'O','O','O'],
            positions=[
                [0.00496714,      -0.00138264,       0.00647689],
                [1.99196588,       1.97439405,       1.97439421],
                [1.99252771,       1.98440993,      -0.00469474],
                [1.98216118,      -0.00463418,       1.97207828],
                [0.00241962,       1.95760278,       1.95948640]
            ],
            cell=[
                [3.9534711604,       0.0000000000,       0.0000000000],
                [0.0000000000,       3.9534711604,       0.0000000000],
                [0.0000000000,       0.0000000000,       3.9534711604]
            ]
    )
    supercell_atoms, reps = make_supercell_ase(
        atoms, minimal_dimension=parsed_args.minimal_dimension,
        rattle_sigma=parsed_args.rattle_sigma
    )
    print supercell_atoms.cell
    print reps
    view(supercell_atoms)


    try:
        from aiida.backends.utils import load_dbenv
        load_dbenv('verdi')
        from aiida.orm.data.structure import StructureData
        sdata = StructureData(ase=atoms)

        supercell_aiida, reps = make_supercell_aiida(
            sdata, minimal_dimension=parsed_args.minimal_dimension,
            rattle_sigma=parsed_args.rattle_sigma
        )   
        view(supercell_aiida.get_ase())
        attrs = supercell_aiida.get_attrs()
        
        del attrs['kinds']
        del attrs['sites']
        print attrs
        print reps
    except Exception as e:
        print e
