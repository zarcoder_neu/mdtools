# -*- coding: utf-8 -*-
__author__ = "Leonid Kahle"
__license__ = "MIT license, see LICENSE.txt file."
__version__ = "0.0.1"
__email__ = "leonid.kahle@epfl.ch"
#!/usr/bin/env python

"""
Submits, updates, retrieves all jobs and optionally ticks the workflow engine
in given intervals.
Basically does what the daemon has to do in the frontend.
"""

from time import sleep
from argparse import ArgumentParser
import sys


if __name__ == '__main__':
    parser = ArgumentParser("""
    Submits, updates andretrieves running calculations in a set interval.
    Optionally also ticks the workflow engine.
    python tick_submit_update_retrieve_now.py
    """)
    parser.add_argument(
            '-l', '--loop', type=float, default=-1,
            help='Iterate over the ticks, with this value to sleep in between'
        )
    parser.add_argument(
            '-t', '--tick', action='store_true', default=False,
            help='Tick the workflow engine, default=False',
    )
    parser.add_argument(
            '-s', '--submit', action='store_true', default=False,
            help='Submit the jobs, default=False',
    )
    parser.add_argument(
            '-u', '--update', action='store_true', default=False,
            help='Update the jobs, default=False',
    )
    parser.add_argument(
            '-r', '--retrieve', action='store_true', default=False,
            help='Retrieve the jobs, default=False',
    )
    parser.add_argument(
            '-a', '--all', action='store_true', default=False,
            help='Submit, update, retrieve the jobs and tick, default=False',
    )
    parser.add_argument(
            '-v', '--verbosity', action='store_true', default=False,
            help='Verbose output',
    )
    parsed_args = parser.parse_args(sys.argv[1:])

    from aiida.backends.utils import load_dbenv
    load_dbenv('verdi')
    from aiida.daemon.execmanager import submit_jobs, retrieve_jobs, update_jobs
    from aiida.workflows2.daemon import tick_workflow_engine

    while True:
        try:
            if parsed_args.submit or parsed_args.all:
                if parsed_args.verbosity:
                    print "   Submitting"
                submit_jobs()
            if parsed_args.update or parsed_args.all:
                if parsed_args.verbosity:
                    print "   Updating"
                update_jobs()
            if parsed_args.retrieve or parsed_args.all:
                if parsed_args.verbosity:
                    print "   Retrieving"
                retrieve_jobs()
            if parsed_args.tick or parsed_args.all:
                if parsed_args.verbosity:
                    print "   Ticking"
                tick_workflow_engine()
        except Exception as e:
            print e
        finally:
            if parsed_args.loop > 0:
                sleep(parsed_args.loop)
            else:
                break


